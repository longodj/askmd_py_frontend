=================
askmd_py_frontend
=================


FrontEnd for AskMD


Description
===========

src/askmd_py_frontend/askmd.py -q "Where is the uncoupling protein 2?"

or

src/askmd_py_frontend/askmd.py


Note
====

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.
