#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
[options.entry_points] section in setup.cfg:

    console_scripts =
         fibonacci = askmd_py_frontend.skeleton:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""

import argparse
import sys
import logging
import copy
import json
import requests
from collections import deque

from xml.etree import ElementTree as ET
from gremlin_python import statics
from gremlin_python.driver.driver_remote_connection import (
    DriverRemoteConnection
)
from gremlin_python.process.graph_traversal import __
from gremlin_python.process.strategies import *
from gremlin_python.process.traversal import Operator, P, T
from gremlin_python.structure.graph import Graph

from askmd_py_frontend import __version__

__author__ = "David Longo"
__copyright__ = "David Longo"
__license__ = "gpl3"

_logger = logging.getLogger(__name__)


class GraphSearch:
    def __init__(self, start, end, relation: str = None):
        graph = Graph()

        self._g = graph.traversal().withRemote(
            DriverRemoteConnection(
                'ws://localhost:8182/gremlin',
                'g'
            )
        )

        print("Starting Graph Search with:")
        print("\tStart: {}".format(start))
        print("\tEnd: {}".format(end))

        self._state = deque()
        self._state.appendleft(start)
        self._end = end
        self._visited = []
        self._frontier = deque()
        self._frontier.appendleft(self._state)
        self._relation = relation

    def actions(self, s):
        retActions = []
        g = self._g
        relation = self._relation
        direction = "both"

        if direction == "both":
            if relation != None:
                frontier = g.V(s[0]).bothE(relation).toList()
            else:
                frontier = g.V(s[0]).bothE().toList()
        elif direction == "in":
            if relation != None:
                frontier = g.V(s[0]).inE(relation).toList()
            else:
                frontier = g.V(s[0]).inE().toList()
        else:
            if relation != None:
                frontier = g.V(s[0]).outE(relation).toList()
            else:
                frontier = g.V(s[0]).outE().toList()

        for nextEdge in frontier:
            retActions.append(nextEdge)

        #print("Next actions: {}".format(retActions))

        return retActions

    def succ(self,s):
        retList = []
        #print("Finding successors for {}".format(s[0]))
        #sys.stdout.write("\t")
        for action in self.actions(s):
            nextMove = copy.deepcopy(s)

            if action.outV.id != s[0]:
                nextV = action.outV
            else:
                nextV = action.inV

            if nextV in self._visited:
                continue
            if nextV is s[0]:
                continue

            nextMove.appendleft(action.label)
            nextMove.appendleft(nextV.id)
            #print(nextMove)
            #sys.stdout.write("{} . ".format(nextMove[0]))
            retList.append(nextMove)
        #sys.stdout.write("\n")
        #sys.stdout.flush()

        #print("Successors:")
        #sys.stdout.write("\t")
        #for path in retList:
        #    sys.stdout.write("{} . ".format(path[0]))
        #sys.stdout.flush()
        #print()

        return retList

    def isEnd(self, s):
        if isinstance(s[0],str) and isinstance(self._end, str) and (s[0] == self._end):
            return True
        elif (not isinstance(s[0],str)) and (s[0].id == self._end):
            return True
        return False

    def bfs(self):
        _frontier = self._frontier
        _visited = self._visited
        _g = self._g

        s = self._state
        e = self._end

        if len(_g.V(s[0]).toList()) == 0 or len(_g.V(e).toList()) == 0:
            return None

        while _frontier:
            #sys.stdout.write("\t")
            #for x in _frontier:
                #sys.stdout.write("{} . ".format(x[0]))
            #sys.stdout.flush()
            #print()
            nextState = _frontier.popleft()

            if self.isEnd(nextState):
                #print("Found path: {}".format(nextState))
                return nextState

            if nextState[0] not in _visited:
                #print("Current State: {}".format(nextState[0]))
                _visited.append(nextState[0])
                _frontier.extend(self.succ(nextState))

        return None

def printPath(path):
    if path == None:
        return ''
    graph = Graph()

    g = graph.traversal().withRemote(
        DriverRemoteConnection(
            'ws://localhost:8182/gremlin',
            'g'
        )
    )
    str = ''

    for i in range(0,len(path)):
        print(path[i])
        if i % 2 == 0:
            str+="{}".format(g.V().hasId(path[i]).values('name').next())
        else:
            str+=("<->")
            str+=(path[i])
            str+=("<->")
        str+=(" ")

    return str

def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Just a Fibonnaci demonstration")
    parser.add_argument(
        '-q',
        dest="question",
        type=str)
    parser.add_argument(
        '--version',
        action='version',
        version='askmd_py_frontend {ver}'.format(ver=__version__))
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    # _logger.debug("Starting crazy calculations...")
    if args.question == None:
        question = input("What would you like to know? ")
    else:
        question = args.question
    print("OK, let's see what we can find out about your question:")
    print("\t\"{}\"".format(question))

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post("http://ec2-3-80-60-144.compute-1.amazonaws.com:5000/askmd/api/v1.0/semrep", data=json.dumps({'article': question}), headers=headers)

    xml = ET.fromstring(json.loads(r.text)['semrep'])

    '''
    Note: This is repeated code from src/askmd_backend/extractor/extractor.py
    '''

    entities = {}
    predications = []
    for u in xml.findall(".//Utterance"):
        sentence = (u.get("id"), u.get("text"))
        for e in u:
            if "Entity" in e.tag:
                entities[e.get("id")] = {
                    "cui"     : e.get("cui"),
                    "name"    : e.get("name"),
                    "semtype" : e.get("semtypes"),
                    "text"    : e.get("text"),
                    "score"   : e.get("score"),
                    "begin"   : e.get("begin"),
                    "end"     : e.get("end")
                }
            if "Predication" in e.tag:
                for p in e:
                    if "Subject" in p.tag:
                        subject = entities[p.get("entityID")]["cui"]
                    if "Predicate" in p.tag:
                        link = (p.get("indicatorType"), p.get("type"))
                    if "Object" in p.tag:
                        object = entities[p.get("entityID")]["cui"]
                predications.append((subject, link, object, sentence))

    _logger.info("Semantic representation processed")


    def CUI_in_Entities(cui, entities):
        for entity in entities:
            features = entities[entity]
            if "cui" in features.keys() and features["cui"] == cui:
                return features["cui"]
        return None

    relation = None
    if "Where" in question or "where" in question:
        relation = "LOCATION_OF"

        graph = Graph()

        g = graph.traversal().withRemote(
            DriverRemoteConnection(
                'ws://localhost:8182/gremlin',
                'g'
            )
        )

        for entity in entities:
            features = entities[entity]
            if "name" in features.keys() and "cui" in features.keys():
                locations = set([e.outV for e in g.V(entities[entity]["cui"]).bothE(relation).toList()])
                print("Found {} locations".format(len(locations)))

                for location in locations:
                    print("{} is found in {}".format(entities[entity]["name"], g.V(location).values("name").next()))


    patients = CUI_in_Entities("C0030705", entities)
    vaccination = CUI_in_Entities("C0042196", entities)
    vaccines = CUI_in_Entities("C0042210", entities)
    if vaccination is not None or vaccines is not None:
        ailments = []
        print("Ailments: ")
        for (subject, link, object, sentence) in predications:
            print("Object, Subject - {}, {}".format(object, subject))
            if object == vaccination or object == vaccines or \
                subject == vaccination or subject == vaccines:
                ailments.append(subject)
                #print("\t{}".format(subject))

                variants = {vaccines, vaccination}
                path = None
                print("Checking {} - {}".format(object,subject))
                for variant in variants:
                    if path != None:
                        break

                    if object == vaccination or object == vaccines:
                        gs = GraphSearch(variant, subject, relation)
                    else:
                        gs = GraphSearch(object, variant, relation)
                    path = gs.bfs()
                print(printPath(path))
                #print(g.V(patients).inE("PROCESS_OF").where(__.outV().hasId(subject)).toList())
    #print(patients)
    if patients is not None:
        symptoms = []
        print("Symptoms: ")
        for (subject, link, object, sentence) in predications:
            if object == patients:
                symptoms.append(subject)
                #print("\t{}".format(subject))

                gs = GraphSearch(patients, subject, relation)
                path = gs.bfs()
                print(printPath(path))
                #print(g.V(patients).inE("PROCESS_OF").where(__.outV().hasId(subject)).toList())

    #print(g.V(patients).outE().where(__.inV().hasId("Riley")).path().toList())

    print(ET.tostring(xml))
    _logger.info("Script ends here")


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
